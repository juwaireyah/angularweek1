import { Component, OnInit, Input,ViewChild,Inject } from '@angular/core';
import {coerceNumberProperty} from '@angular/cdk/coercion';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { DatePipe } from '@angular/common';
import {Dish} from '../shared/dish';
//import {DISHES} from '../shared/dishes';
import { DishService } from '../services/dish.service';
import {Params, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common'; //to track history of browser
import {switchMap} from 'rxjs/operators';
import {visibility, flyInOut, expand} from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host:{
    '[@flyInOut]': 'true',
    'style':'display: block;'
  },
  animations:[
    flyInOut(),
    visibility(),
    expand()
  ]
})

export class DishdetailComponent implements OnInit {
  addCommentForm:FormGroup;
   // @Input()
  dish:Dish; // dish: Dish[];
  errMess: string;
  dishIds: string[];
  prev:string;
  next: string;
  @ViewChild('cform') commentFormDirective;
  value = 5;
  dishcopy: Dish;

  visibility = 'shown';

  formErrors = {
    'author':'',
    'rating':'',
    'comment':''
  };

  validationMessages = {
    'author':{
      'required':'Author name is required',
      'minlength': 'Author name must be at least 2 characters long'
     // 'maxlength':'Author name cannot be more than 25 characters long'
    },
    'comment':{
      'required':'Comment is required'
    },
    'rating':{
      'required':'rating is required'
    }
  };

   //dish:DishService
  //today: number = Date.now();
  date = new Date();
  constructor(private dishService:DishService,
    private location:Location, private route:ActivatedRoute,private fb:FormBuilder, @Inject('BaseURL') private BaseURL) {
      this.createForm();
  }

  ngOnInit() {
  this.dishService.getDishIds().subscribe((dishIds)=> this.dishIds = dishIds);
   this.route.params.pipe(switchMap((params: Params)=>{this.visibility = 'hidden'; return this.dishService.getDish(params['id']); }))//id of this dish from router link param
    .subscribe((dishes) => { this.dish = dishes;this.dishcopy = dishes; this.setPrevNext(this.dish.id); this.visibility =  'shown'; },
    errmess => this.errMess = <any>errmess);
     console.log(this.dishIds);
  }

  createForm(){
    this.addCommentForm = this.fb.group({
      author:['',[Validators.required, Validators.minLength(2)]],
      rating:[5,Validators.required],
      comment:['',Validators.required]
    });

    this.addCommentForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); //reset form validation messages

  }

  setPrevNext(dishId: string){
    const index = this.dishIds.indexOf(dishId);
    //find the previous item in array. if current item is at index 0, wrap around (go back to the last)
    this.prev = this.dishIds[(this.dishIds.length + index -1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];

  }

  goBack():void{
    this.location.back();
  }

  onSubmit(){
    console.log("button clicked!");
    console.log(this.addCommentForm);
    var date = new Date();
    this.dishcopy.comments.push({rating:this.addCommentForm.get('rating').value,comment:this.addCommentForm.get('comment').value,author:this.addCommentForm.get('author').value,date:date.toISOString()});
    this.dishService.putDish(this.dishcopy)
      .subscribe(dish=>{
        this.dish = dish;
        this.dishcopy = dish;
      },
      errmess=> {this.dish = null;this.dishcopy = null;
        this.errMess = <any>errmess; 
    });

    this.addCommentForm.reset({
      author:'',
      rating:5,
      comment:''
    });
    this.commentFormDirective.resetForm();
   // this.addCommentForm.controls['rating'].setValue(5);
 //  this.form.controls.dept.setValue(selected.id);
   this.addCommentForm.get('rating').setValue(5);
    console.log(this.addCommentForm.get('rating').value);
  }

  onValueChanged(data?: any){
    console.log( this.addCommentForm.get('author').value);
    if(!this.addCommentForm){return ;}
    const form = this.addCommentForm;

    for (const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        //clear previous error mssg (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
          const messages =  this.validationMessages[field];
          for (const key in control.errors){
            if (control.errors.hasOwnProperty(key)){
              this.formErrors[field] += messages[key];
            }
          }
        }
      }
    }
    console.log(this.dish);
    if (this.dish && this.addCommentForm.valid && this.addCommentForm.dirty){
      
     // this.dish.comments.push({rating:this.addCommentForm.get('rating').value,comment:this.addCommentForm.get('comment').value,author:this.addCommentForm.get('author').value,date:date.toISOString()});
    }
  }

 
}
